import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-card-container',
  templateUrl: './card-container.component.html',
  styleUrls: ['./card-container.component.css']
})
export class CardContainerComponent implements OnInit , OnDestroy{

  dataArr = [];
  constructor(private apiService: ApiService) {
    this.apiService.getFilter$.subscribe(d => {
      this.dataArr = d;
    })
   }

  ngOnInit(): void {
    this.getDataFromLoading();
  }

  ngOnDestroy(): void{
  }

  getDataFromLoading(){
    this.apiService.getDataFromLoading().subscribe(d => {
       console.log(d);
       if(d && d.length && d.length > 0){
        this.dataArr = d;
       }       
    },err =>{})
  }

}
