import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  cardData: any;

  @Input('cardV') set setValue(value){
    if(value){
      this.cardData = value;
    }
  }
  constructor() { }

  ngOnInit(): void {
  }

}
