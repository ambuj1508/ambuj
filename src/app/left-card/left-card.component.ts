import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-left-card',
  templateUrl: './left-card.component.html',
  styleUrls: ['./left-card.component.css']
})
export class LeftCardComponent implements OnInit {
  dataArr = [];
  yearArr = ['2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020']
  selectedYear = '2006';
  obj = {};
  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
  }

  getDataFromLoading(type,value){
    
    if(type === 'filter'){
      this.selectedYear = value;
      this.obj = {...this.obj,...{launch_year: value}}
    } else if(type === 'launch_success'){
      this.obj = {...this.obj,...{launch_success: value}}
    } else{
      this.obj = {...this.obj,...{land_success: value}}
    }
    this.apiService.getDataFromLoading(this.obj).subscribe(d => {
       console.log(d);
       if(d && d.length && d.length > 0){
        this.apiService.setFilter(d);
       } else{
        this.apiService.setFilter([]);
       }      
    },err =>{})
  }

}
