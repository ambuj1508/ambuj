import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponent } from './card/card.component';
import { LeftCardComponent } from './left-card/left-card.component';
import { CardContainerComponent } from './card-container/card-container.component';
import { ApiService } from './api.service';

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    LeftCardComponent,
    CardContainerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
