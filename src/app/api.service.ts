import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }
  public getFilter: BehaviorSubject<any> = new BehaviorSubject<any>(undefined);

  getFilter$ = this.getFilter.asObservable();


  public getDataFromLoading(params = {}): Observable<any>{
    return this.httpClient.get(`https://api.spaceXdata.com/v3/launches?limit=100`,{params});
  }

  public setFilter(value) {
    this.getFilter.next(value);
  }

}
